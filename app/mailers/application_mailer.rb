class ApplicationMailer < ActionMailer::Base
  default from: "info@btoa-company.com"
  layout 'mailer'
end
